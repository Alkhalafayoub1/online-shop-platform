package com.example.shoppingplatfrom.product.controller;

import com.example.shoppingplatfrom.plan.dto.PlanRequest;
import com.example.shoppingplatfrom.product.dto.ProductRequest;
import com.example.shoppingplatfrom.product.dto.ProductResponse;
import com.example.shoppingplatfrom.product.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
@Slf4j
@RestController
@RequestMapping("/api/product")
@RequiredArgsConstructor
public class ProductController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);
 private  final ProductService productService;
 private final ModelMapper mapper;
    @PostMapping("/createproduct")
    public ResponseEntity<?> createProduct(@RequestBody ProductRequest productRequest){
        productService.createProduct(productRequest);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
    @GetMapping("/get-products")
    public List<ProductResponse> getProducts(Pageable pageable) {
        return productService.getProducts( pageable);

    }
    @PutMapping("/{id}")
    public void putProduct(
            @PathVariable("id") String id,
            @RequestBody ProductRequest productRequest
    ) {

        productService.updateProduct(id, productRequest);
    }
}
