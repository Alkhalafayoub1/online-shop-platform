package com.example.shoppingplatfrom.product.dto;

import com.example.shoppingplatfrom.category.dto.CategoryRequest;
import com.example.shoppingplatfrom.domainpermitive.dto.DiscountRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductRequest {

    private String name;
    private String description;
    private CategoryRequest categoryRequest;
    private double price;
    private Integer quantity;
    private DiscountRequest productDiscount;
    private String createdDate;
    private String modifiedDate;
    private String endedDate;

}
