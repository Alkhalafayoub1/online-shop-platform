package com.example.shoppingplatfrom.product.dto;

import com.example.shoppingplatfrom.category.model.Category;
import com.example.shoppingplatfrom.domainpermitive.model.Discount;
import com.example.shoppingplatfrom.product.model.ProductStatus;
import com.example.shoppingplatfrom.vendor.model.Vendor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductResponse {
    private String id;
    private String name;
    private Vendor vendor;
    private double price;
    private String description;
    private Category category;
    private Integer quantity;
    private ProductStatus productStatus;
    private Discount productDiscount;
    private String createdDate;
    private String modifiedDate;
    private String endedDate;


}
