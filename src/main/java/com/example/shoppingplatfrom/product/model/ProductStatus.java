package com.example.shoppingplatfrom.product.model;

public enum ProductStatus {
    DELETED,
    SOLD,
    ALMOSTSOLD,
    NEW,
    MODIFIED
}
