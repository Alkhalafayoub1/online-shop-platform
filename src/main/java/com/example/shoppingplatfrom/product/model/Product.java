package com.example.shoppingplatfrom.product.model;


import com.example.shoppingplatfrom.category.model.Category;
import com.example.shoppingplatfrom.domainpermitive.model.Discount;
import com.example.shoppingplatfrom.vendor.model.Vendor;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Product {
    @Id
    private String id;
    @Indexed(unique = true)
    private String name;
    @DBRef
    private Vendor vendor;
    private double price;
    private String description;
    @DBRef
    private Category category;
    private Integer quantity;
    private ProductStatus productStatus;
    private Discount productDiscount;
    private String createdDate;
    private String modifiedDate;
    private String endedDate;


}
