package com.example.shoppingplatfrom.product.repository;

import com.example.shoppingplatfrom.product.model.Product;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface ProductRepository extends MongoRepository<Product,String> {

    Optional<Product> findById(String id);


}
