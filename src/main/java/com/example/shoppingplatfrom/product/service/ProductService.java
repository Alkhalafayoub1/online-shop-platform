package com.example.shoppingplatfrom.product.service;

import com.example.shoppingplatfrom.User.model.User;
import com.example.shoppingplatfrom.User.repository.UserRepository;
import com.example.shoppingplatfrom.category.model.Category;
import com.example.shoppingplatfrom.category.repository.CategoryRepository;
import com.example.shoppingplatfrom.domainpermitive.exception.NotFoundException;
import com.example.shoppingplatfrom.plan.dto.PlanRequest;
import com.example.shoppingplatfrom.plan.dto.PlanResponse;
import com.example.shoppingplatfrom.plan.model.Plan;
import com.example.shoppingplatfrom.product.dto.ProductRequest;
import com.example.shoppingplatfrom.product.dto.ProductResponse;
import com.example.shoppingplatfrom.product.model.Product;
import com.example.shoppingplatfrom.product.repository.ProductRepository;
import com.example.shoppingplatfrom.vendor.model.Vendor;
import com.example.shoppingplatfrom.vendor.repository.VendorRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
@Slf4j
@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;
    private final UserRepository userRepository;
    private final VendorRepository vendorRepository;
    private final CategoryRepository categoryRepository;

    private UserDetailsService userDetailsService;
    private final ModelMapper mapper;

    public void createProduct(ProductRequest productRequest) {
        UserDetails user = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user1 = findOrThrow(user.getUsername());
        Vendor vendor = findOrThrow1(user1);
        Category category=findCategoryOrThrow(productRequest.getCategoryRequest().getName());
        Product product = Product.builder()
                .name(productRequest.getName())
                .description(productRequest.getDescription())
                .price(productRequest.getPrice())
                .vendor(vendor)
                .category(category)
                .createdDate(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss z").format(new Date()))
                .build();
        productRepository.save(product);
    }
    private Product convertToProduct(ProductRequest productRequest) {
        return mapper.map(productRequest, Product.class);
    }

    private User findOrThrow(String username) {
        return userRepository
                .findByEmail(username)
                .orElseThrow(() -> new NotFoundException("User by " + username + " was not found"));
    }

    private Vendor findOrThrow1(User user) {
        return vendorRepository
                .findByUserId(user.getId())
                .orElseThrow(() -> new NotFoundException("Vendor by email " + user.getEmail() + " was not found"));
    }

    private ProductResponse convertToDto(Product product) {
        return mapper.map(product, ProductResponse.class);
    }

    public List<ProductResponse> getProducts(Pageable pageable) {
        int toSkip = pageable.getPageSize() * pageable.getPageNumber();
        //SLF4J

        log.info("Using SLF4J Lombok: Getting Product list");
        // Mapstruct is another dto mapper, but it's not straight forward
        var ProductList = StreamSupport
                .stream(findAllProducts().spliterator(), false)
                .filter(product -> product.getQuantity()>0)
                .skip(toSkip).limit(pageable.getPageSize())
                .toList();


        return ProductList
                .stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }
    public Iterable<Product> findAllProducts() {
        return productRepository.findAll();

    }
    public void updateProduct(String id, ProductRequest productRequest) {
        var product = findProductOrThrow(id);


        if (productRequest.getProductDiscount().getDiscountPercent()>0 ){
            product.setPrice(
                    product.getPrice()-
                            (((productRequest.getPrice())
                                    *(productRequest.getProductDiscount().getDiscountPercent()))/100));
        }else {
            product.setPrice(productRequest.getPrice());
        }
        product.setDescription(productRequest.getDescription());
        var category= findCategoryOrThrow(productRequest.getCategoryRequest().getName());
        product.setCategory(category);
        product.setName(productRequest.getName());

        product.setModifiedDate(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss z")
                .format(new Date()));
        product.setQuantity(productRequest.getQuantity());
        productRepository.save(product);
    }
    private Category findCategoryOrThrow(String name) {
        return  categoryRepository
                .findByName(name)
                .orElseThrow(() -> new NotFoundException("Category by name " + name + " was not found"));
    }
    private Product findProductOrThrow(String id) {
        return  productRepository
                .findById(id)
                .orElseThrow(() -> new NotFoundException("Product by id " + id + " was not found"));
    }
}

