package com.example.shoppingplatfrom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShoppingPlatfromApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShoppingPlatfromApplication.class, args);
    }

}
