package com.example.shoppingplatfrom.Favourites.Model;

import com.example.shoppingplatfrom.product.model.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.catalina.User;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Favourites {
    @Id
    @Indexed(unique = true)
    private String id;
    private User user;
    @DBRef
    List<Product> products;
}
