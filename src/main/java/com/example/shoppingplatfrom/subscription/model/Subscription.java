package com.example.shoppingplatfrom.subscription.model;

import com.example.shoppingplatfrom.plan.model.Plan;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Subscription {
    @Id
    @Indexed(unique = true)
    private String id;
    @DBRef
    private Plan plan;

    private Date createdDate;
    private Date startedDate;
    private Date endDate;







}
