package com.example.shoppingplatfrom.subscription.repository;

import com.example.shoppingplatfrom.subscription.model.Subscription;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SubscriptionRepository extends MongoRepository<Subscription,String> {
}
