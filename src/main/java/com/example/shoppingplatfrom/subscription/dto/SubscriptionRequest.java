package com.example.shoppingplatfrom.subscription.dto;

import com.example.shoppingplatfrom.plan.dto.PlanRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SubscriptionRequest {


    private PlanRequest planRequest;

    private String createdDate;
    private String modifiedDate;
    private String endedDate;







}
