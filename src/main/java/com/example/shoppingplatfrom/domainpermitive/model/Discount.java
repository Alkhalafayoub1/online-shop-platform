package com.example.shoppingplatfrom.domainpermitive.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Discount {

    private String id;
    private String discountCode;
    private double discountPercent;
    private String description;
    private Date createdDate;
    private Date modifiedDate;
    private Date endedDate;


}
