package com.example.shoppingplatfrom.domainpermitive.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Comment {

    private String id;
    private String text;
    private Date DateComment;
    
}
