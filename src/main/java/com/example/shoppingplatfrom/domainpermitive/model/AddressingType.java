package com.example.shoppingplatfrom.domainpermitive.model;

public enum AddressingType {
    USER_ADDRESS,
    BILLING_ADDRESS,
    SHIPPING_ADDRESS
}
