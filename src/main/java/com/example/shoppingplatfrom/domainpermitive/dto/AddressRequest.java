package com.example.shoppingplatfrom.domainpermitive.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AddressRequest {
    private String city;
    private String street;
    private String zipCode;
    private String houseNr;
    private String Country;

}
