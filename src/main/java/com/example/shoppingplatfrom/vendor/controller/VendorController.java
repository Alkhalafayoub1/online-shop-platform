package com.example.shoppingplatfrom.vendor.controller;

import com.example.shoppingplatfrom.vendor.dto.VendorRequest;
import com.example.shoppingplatfrom.vendor.service.VendorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/vendor")
@RequiredArgsConstructor
public class VendorController {
 private  final VendorService  vendorService;
 @PostMapping("/createvendor")
 public ResponseEntity<?> createPlan(@RequestBody VendorRequest vendorRequest){
  vendorService.createVendor(vendorRequest);
  return ResponseEntity.status(HttpStatus.CREATED).build();
 }
 @PutMapping("/{id}")
 public void putVendor(
         @PathVariable("id") String id,
         @RequestBody VendorRequest   vendorRequest
 ) {

  vendorService.updateVendor(id, vendorRequest);
 }
}
