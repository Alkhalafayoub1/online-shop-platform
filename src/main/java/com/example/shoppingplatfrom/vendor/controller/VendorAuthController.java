package com.example.shoppingplatfrom.vendor.controller;


import com.example.shoppingplatfrom.User.service.AuthenticationService;
import com.example.shoppingplatfrom.vendor.dto.VendorRequest;
import com.example.shoppingplatfrom.vendor.model.Vendor;
import com.example.shoppingplatfrom.vendor.service.VendorService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/vendor/auth")
@RequiredArgsConstructor
public class VendorAuthController {
    private final VendorService vendorService;
    private final AuthenticationService authenticationService;
    @PostMapping("/register")
    public ResponseEntity<Vendor> register(@RequestBody VendorRequest vendorRequest){
       // return ResponseEntity.ok(vendorService.registerVendor(vendorRequest));
        return ResponseEntity.ok(vendorService.registerVendor(vendorRequest));
    }

  //  @PostMapping("/authenticate")
  //  public ResponseEntity<AuthenticationResponse> authenticate(@RequestBody AuthenticationRequest request){
  //      return ResponseEntity.ok(authenticationService.authenticate(request));
  //  }
}
