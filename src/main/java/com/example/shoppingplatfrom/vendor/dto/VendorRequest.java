package com.example.shoppingplatfrom.vendor.dto;

import com.example.shoppingplatfrom.User.auth.RegisterRequest;
import com.example.shoppingplatfrom.domainpermitive.dto.AddressRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class VendorRequest {

    private RegisterRequest registerRequest;
    private AddressRequest companyAddress;
    private String createdDate;
    private String modifiedDate;
    private String endedDate;
}
