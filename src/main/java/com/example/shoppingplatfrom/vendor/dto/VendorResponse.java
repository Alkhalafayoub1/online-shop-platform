package com.example.shoppingplatfrom.vendor.dto;

import com.example.shoppingplatfrom.User.model.User;
import com.example.shoppingplatfrom.domainpermitive.model.Address;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class VendorResponse {
    private String id;
    private User user;
    private Address companyAddress;
    private String createdDate;
    private String modifiedDate;
    private String endedDate;
}
