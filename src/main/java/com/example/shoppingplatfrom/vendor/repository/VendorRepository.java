package com.example.shoppingplatfrom.vendor.repository;

import com.example.shoppingplatfrom.vendor.model.Vendor;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface VendorRepository extends MongoRepository<Vendor,String> {
    public  Optional<Vendor> findByUserId(String id);


}
