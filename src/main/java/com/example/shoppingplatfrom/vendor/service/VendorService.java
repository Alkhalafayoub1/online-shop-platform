package com.example.shoppingplatfrom.vendor.service;

import com.example.shoppingplatfrom.User.auth.RegisterRequest;
import com.example.shoppingplatfrom.User.config.JwtService;
import com.example.shoppingplatfrom.User.model.User;
import com.example.shoppingplatfrom.User.repository.UserRepository;
import com.example.shoppingplatfrom.User.service.AuthenticationService;
import com.example.shoppingplatfrom.domainpermitive.dto.AddressRequest;
import com.example.shoppingplatfrom.domainpermitive.exception.NotFoundException;
import com.example.shoppingplatfrom.domainpermitive.model.Address;
import com.example.shoppingplatfrom.vendor.dto.VendorRequest;
import com.example.shoppingplatfrom.vendor.model.Vendor;
import com.example.shoppingplatfrom.vendor.repository.VendorRepository;
import lombok.RequiredArgsConstructor;


import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class VendorService {
    private final VendorRepository vendorRepository;
    private final UserRepository userRepository;
    private final AuthenticationService authenticationService;
    private final JwtService jwtService;
    private final PasswordEncoder passwordEncoder;
    private final ModelMapper mapper;

    public void createVendor(VendorRequest vendorRequest) {
        var vendor = convertToDocument(vendorRequest);
        vendorRepository.save(vendor);
    }

    private Vendor convertToDocument(VendorRequest vendorRequest) {
        return mapper.map(vendorRequest, Vendor.class);
    }

    public void updateVendor(String id, VendorRequest vendorRequest) {
        findOrThrow(id);
        var vendor = mapper.map(vendorRequest, Vendor.class);
        vendor.setId(id);
        vendorRepository.save(vendor);

    }

    private Vendor findOrThrow(final String id) {
        return vendorRepository
                .findById(id)
                .orElseThrow(
                        () -> new NotFoundException("Vendor by id " + id + " was not found")
                );
    }

    public Vendor registerVendor(VendorRequest vendorRequest) {
        var vendor=Vendor.builder()
                .user(comvertToUser(vendorRequest.getRegisterRequest()))
                .companyAddress(convertToAddress(vendorRequest.getCompanyAddress()))
                        .build();
        userRepository.save(vendor.getUser());
        vendorRepository.save(vendor);
        return vendor;

    }

    private User comvertToUser(RegisterRequest registerRequest) {
        return User.builder()
                .email(registerRequest.getEmail())
                .firstName(registerRequest.getFirstName())
                .lastName(registerRequest.getLastName())
                .password(passwordEncoder.encode(registerRequest.getPassword()))
                .role(registerRequest.getRole())
                .address(convertToAddress(registerRequest.getAddressRequest()))
                .userStatus(registerRequest.getUserStatus())
                .build();
    }
    public Address convertToAddress(AddressRequest addressRequest){
        var address=mapper.map(addressRequest, Address.class);
      //  address.setId(UUID.randomUUID().toString());
        return address;
    }


}