package com.example.shoppingplatfrom.vendor.model;


import com.example.shoppingplatfrom.User.model.User;
import com.example.shoppingplatfrom.domainpermitive.model.Address;
import com.example.shoppingplatfrom.subscription.model.Subscription;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.DocumentReference;

import java.util.List;

@Document(collection = "vendor")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Vendor {
    @Id
    @Indexed(unique = true)
    private String id;
    @DocumentReference
    private User user;
    @DocumentReference
    List<Subscription>  subscriptions;


    private Address companyAddress;
}
