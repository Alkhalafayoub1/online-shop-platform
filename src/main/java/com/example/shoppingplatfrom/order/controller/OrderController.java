package com.example.shoppingplatfrom.order.controller;

import com.example.shoppingplatfrom.order.dto.OrderRequest;
import com.example.shoppingplatfrom.order.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/order")
@RequiredArgsConstructor
public class OrderController {
    private final OrderService orderService;
    @PostMapping("/create-order")
    public ResponseEntity<?> createOrder(@RequestBody OrderRequest orderRequest){

        orderService.createOrder(orderRequest);
      return ResponseEntity.ok(HttpStatus.CREATED);
    }
}
