package com.example.shoppingplatfrom.order.repository;

import com.example.shoppingplatfrom.order.model.Order;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface OrderRepository extends MongoRepository<Order,String> {
}
