package com.example.shoppingplatfrom.order.model;

import com.example.shoppingplatfrom.product.model.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderItem {

    private Product product;
    private Integer quantity;
}
