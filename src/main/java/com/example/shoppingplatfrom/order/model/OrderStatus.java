package com.example.shoppingplatfrom.order.model;

public enum OrderStatus {
    New,
    DELIVERED,
    HOLED,
    SHIPPED,
    CLOSED
}
