package com.example.shoppingplatfrom.order.model;


import com.example.shoppingplatfrom.User.model.User;

import com.example.shoppingplatfrom.domainpermitive.model.Address;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Order {
    @Id
    @Indexed(unique = true)
    private String id;
    @DBRef
    private User user;

    private List<OrderItem> orderItemList=new ArrayList<>();
    private Address shippingAddress;
    private BigDecimal totalPrice;
    private String createdAt;



}
