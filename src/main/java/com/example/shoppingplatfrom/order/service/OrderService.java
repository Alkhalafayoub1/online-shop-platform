package com.example.shoppingplatfrom.order.service;

import com.example.shoppingplatfrom.User.model.User;
import com.example.shoppingplatfrom.User.repository.UserRepository;
import com.example.shoppingplatfrom.cart.model.Cart;
import com.example.shoppingplatfrom.cart.model.CartItem;
import com.example.shoppingplatfrom.cart.repository.CartRepository;
import com.example.shoppingplatfrom.domainpermitive.dto.AddressRequest;
import com.example.shoppingplatfrom.domainpermitive.exception.NotFoundException;
import com.example.shoppingplatfrom.domainpermitive.model.Address;
import com.example.shoppingplatfrom.order.dto.OrderRequest;
import com.example.shoppingplatfrom.order.model.Order;
import com.example.shoppingplatfrom.order.model.OrderItem;
import com.example.shoppingplatfrom.order.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;
    private final ModelMapper mapper;
    private final UserRepository userRepository;
    private final CartRepository cartRepository;
    public void createOrder(OrderRequest orderRequest) {
        UserDetails user = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user1 = findOrThrow(user.getUsername());
        Optional<Cart> cart = cartRepository.findByUser(user1);
        if (cart.isPresent()){
            List<OrderItem> orderItems=cart.get().getCartItems()
                    .stream()
                    .map(this::convertToOrderItem)
                    .toList();
            Order  order=Order.builder()
                    .user(user1)
                    .orderItemList(orderItems)
                    .createdAt(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss z")
                            .format(new Date()))
                    .shippingAddress(convertToAddress(orderRequest.getAddressRequest()))
                    .build();
            orderRepository.save(order);
            //cartRepository.delete(cart.get());
            cart.get().getCartItems().clear();
            cart.get().setTotal(0.0);
        }



    }

    private OrderItem convertToOrderItem(CartItem cartItem) {
        return OrderItem.builder()
                .product(cartItem.getProduct())
                .quantity(cartItem.getQuantity())
                .build();
    }
    private Address convertToAddress(AddressRequest addressRequest){

        return mapper.map(addressRequest,Address.class);
    }

    private User findOrThrow(String username) {
        return userRepository
                .findByEmail(username)
                .orElseThrow(
                        () -> new NotFoundException("User by " + username + " was not found"));
    }
    private Cart findCartOrThrow(String id) {
        return cartRepository
                .findById(id)
                .orElseThrow(
                        () -> new NotFoundException("Cart by " + id + " was not found"));
    }
}
