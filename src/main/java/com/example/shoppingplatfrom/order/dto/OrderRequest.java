package com.example.shoppingplatfrom.order.dto;

import com.example.shoppingplatfrom.cart.model.Cart;
import com.example.shoppingplatfrom.domainpermitive.dto.AddressRequest;
import com.example.shoppingplatfrom.domainpermitive.model.Address;
import com.example.shoppingplatfrom.order.model.OrderItem;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderRequest {


     private AddressRequest addressRequest;
}
