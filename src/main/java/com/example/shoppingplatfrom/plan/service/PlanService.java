package com.example.shoppingplatfrom.plan.service;

import com.example.shoppingplatfrom.domainpermitive.exception.NotFoundException;
import com.example.shoppingplatfrom.plan.dto.PlanRequest;
import com.example.shoppingplatfrom.plan.dto.PlanResponse;
import com.example.shoppingplatfrom.plan.model.Plan;
import com.example.shoppingplatfrom.plan.model.PlanStatus;
import com.example.shoppingplatfrom.plan.repository.PlanRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PlanService {
    private final PlanRepository planRepository;
    private final ModelMapper mapper;

    public void createPlan(PlanRequest planRequest) {
        var document = convertToDocument(planRequest);
        document.setCreatedDate(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss z")
                .format(new Date()));
        planRepository.save(document);
    }

    private Plan convertToDocument(PlanRequest dto) {
        return mapper.map(dto, Plan.class);
    }


    public PlanResponse IsInStock(String id) {
        var document = findOrThrow(id);
        return mapper.map(document, PlanResponse.class);


    }

    private Plan findOrThrow(final String id) {
        return planRepository
                .findById(id)
                .orElseThrow(
                        () -> new NotFoundException("Plan by id " + id + " was not found")
                );
    }

    public void removePlanById(String id) {
        Plan plan = findOrThrow(id);
        if (plan == null) throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST,
                "id does not match"
        );
        plan.setEndedDate(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss z")
                .format(new Date()));
        plan.setPlanStatus(PlanStatus.DELETED);
        planRepository.save(plan);
    }

    public void updatePlan(String id, PlanRequest planRequest) {
        Plan plan = findOrThrow(id);


        if (planRequest.getDiscountRequest().getDiscountPercent()>0 ){
            plan.setPrice(
                    planRequest
                            .getPrice()-
                            (((planRequest.getPrice())*(planRequest.getDiscountRequest().getDiscountPercent()))/100));
        }else {
           plan.setPrice(planRequest.getPrice());
        }


        plan.setDescription(planRequest.getDescription());
        plan.setDuration(plan.getDuration());
        plan.setProductCount(planRequest.getProductCount());
        plan.setDurationUnit(planRequest.getDurationUnit());
        plan.setModifiedDate(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss z")
                .format(new Date()));
        plan.setPlanStatus(planRequest.getPlanStatus());
        plan.setDurationUnit(planRequest.getDurationUnit());

        planRepository.save(plan);
    }

    public List<PlanResponse> getAllPlan() {
     return    planRepository.findAll()
                .stream()                                //.filter(plan ->plan.getPlanStatus().equals(PlanStatus.ISUSED))
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    private PlanResponse convertToDto(Plan plan) {
        return mapper.map(plan, PlanResponse.class);
    }
}