package com.example.shoppingplatfrom.plan.model;

import com.example.shoppingplatfrom.domainpermitive.model.Discount;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Plan {
    @Id
    @Indexed(unique = true)
    private String id;
    private double price;
    private String description;
    private int duration;// 15
    private DurationUnit durationUnit;// day

    private int productCount;
    private Discount   discount;
     private PlanStatus planStatus;
    private String createdDate;
    private String modifiedDate;
    private String endedDate;
}
