package com.example.shoppingplatfrom.plan.model;

public enum DurationUnit {
  DAY,
    MONTH,
    YEAR
}
