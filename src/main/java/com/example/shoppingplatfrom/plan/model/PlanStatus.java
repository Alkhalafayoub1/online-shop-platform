package com.example.shoppingplatfrom.plan.model;

public enum PlanStatus {
    ACTIVATED,
    DELETED
}
