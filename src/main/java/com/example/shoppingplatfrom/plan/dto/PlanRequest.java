package com.example.shoppingplatfrom.plan.dto;

import com.example.shoppingplatfrom.domainpermitive.dto.DiscountRequest;
import com.example.shoppingplatfrom.plan.model.DurationUnit;
import com.example.shoppingplatfrom.plan.model.PlanStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PlanRequest {
    private double price;
    private String description;
    private int duration;// 15
    private DurationUnit durationUnit;// day

    private int productCount;
    private DiscountRequest discountRequest;

    private String createdDate;
    private String modifiedDate;
    private String endedDate;
    private PlanStatus planStatus;

}

