package com.example.shoppingplatfrom.plan.dto;

import com.example.shoppingplatfrom.plan.model.PlanStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PlanResponse{
    private String id;
    private double Price;
    private String description;
    private int duration;// 15
    private int productCount;
    private PlanStatus planStatus;
    private String createdDate;
    private String modifiedDate;
    private String endedDate;

}
