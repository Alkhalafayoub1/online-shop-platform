package com.example.shoppingplatfrom.plan.controller;

import com.example.shoppingplatfrom.plan.dto.PlanRequest;
import com.example.shoppingplatfrom.plan.dto.PlanResponse;
import com.example.shoppingplatfrom.plan.service.PlanService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/api/admin/plan")
public class PlanController {
    private final PlanService planService;
    private final ModelMapper modelMapper;

    @PostMapping("/createplan")
    public ResponseEntity<?> createPlan(@RequestBody PlanRequest planRequest){
        planService.createPlan(planRequest);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping
    public List<PlanResponse> getAllPlan(){

        return planService.getAllPlan();
    }

    @GetMapping("/{id}")
    public PlanResponse getPlan(@PathVariable("id") String id) {
        return planService.IsInStock(id);
    }

    @DeleteMapping("/{id}")
    public void deletePlanById(@PathVariable("id") String id) {
        planService.removePlanById(id);
    }

    @PutMapping("/{id}")
    public void putPlan(
            @PathVariable("id") String id,
             @RequestBody PlanRequest   planRequest
    ) {

        planService.updatePlan(id, planRequest);
    }
}
