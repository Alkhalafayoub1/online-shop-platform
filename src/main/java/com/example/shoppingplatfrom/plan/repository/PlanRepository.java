package com.example.shoppingplatfrom.plan.repository;

import com.example.shoppingplatfrom.plan.model.Plan;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PlanRepository extends MongoRepository<Plan,String> {
}
