package com.example.shoppingplatfrom.cart.service;
import com.example.shoppingplatfrom.User.model.User;
import com.example.shoppingplatfrom.User.repository.UserRepository;
import com.example.shoppingplatfrom.cart.dto.CartItemRequest;
import com.example.shoppingplatfrom.cart.dto.CartRequest;
import com.example.shoppingplatfrom.cart.model.Cart;
import com.example.shoppingplatfrom.cart.model.CartItem;
import com.example.shoppingplatfrom.cart.repository.CartRepository;
import com.example.shoppingplatfrom.domainpermitive.exception.NotFoundException;
import com.example.shoppingplatfrom.product.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class CartService {
    private final CartRepository cartRepository;
    private final UserDetailsService userDetailsService;
    private final ProductRepository productRepository;
    private final UserRepository userRepository;
    private final ModelMapper mapper;

    public void addToCart(CartRequest cartRequest) {
        UserDetails user = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user1 = findOrThrow(user.getUsername());
        Optional<Cart> cart = cartRepository.findByUser(user1);
        if (cart.isPresent()) {
            //is product in the Cart
            boolean isProductInCart = false;
            for (CartItem item : cart.get().getCartItems()) {
                if (item.getProduct().getId().equals(cartRequest.getCartItemRequest().getProductCartRequest().getId())) {
                    item.setQuantity(item.getQuantity() + cartRequest.getCartItemRequest().getQuantity());
                    item.setTotal(item.getProduct().getPrice() * item.getQuantity());
                    cart.get().setTotal(item.getTotal());
                    cartRepository.save(cart.get());
                    isProductInCart = true;
                }
            }
            if (!isProductInCart) {

                CartItem cartItem = convertToCartItem(cartRequest.getCartItemRequest());
                cartItem.setTotal(cartItem.getProduct().getPrice() * cartItem.getQuantity());
                cart.get().getCartItems().add(cartItem);
                cart.get().setTotal(cart.get().getTotal()+cartItem.getTotal());
                cartRepository.save(cart.get());
            }

        } else {

            List<CartItem> cartItems = new ArrayList<>();
            CartItem cartItem = convertToCartItem(cartRequest.getCartItemRequest());
            cartItem.setTotal(cartItem.getProduct().getPrice() * cartItem.getQuantity());
            cartItems.add(cartItem);
            Cart cart1 = Cart.builder()
                    .user(user1)
                    .cartItems(cartItems)
                    .total(cartItem.getTotal())
                    .build();
            //cart1.getCartItems().add(convertToCartItem(cartRequest.getCartItemRequest()));
            cartRepository.save(cart1);
        }

    }
    private CartItem convertToCartItem(CartItemRequest cartItemRequest) {
        return mapper.map(cartItemRequest, CartItem.class);
    }
    private User findOrThrow(String username) {
        return userRepository
                .findByEmail(username)
                .orElseThrow(
                        () -> new NotFoundException("User by " + username + " was not found"));

    }
    private Cart findCartOrThrow(String id) {
        return cartRepository
                .findById(id)
                .orElseThrow(
                        () -> new NotFoundException("Cart by " + id + " was not found"));

    }
    public void updateCart(String itemId, String cartId) {
    }

    public void deleteItemFromCart(String id) {
        UserDetails user = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user1 = findOrThrow(user.getUsername());
        Optional<Cart> cart = cartRepository.findByUser(user1);
        if (cart.isPresent()) {
            for (CartItem item : cart.get().getCartItems())
                if (item.getProduct().getId().equals(id)) {
                    cart.get().getCartItems().remove(item);
                    cartRepository.save(cart.get());
                }
        }

    }
    public void modifyQuantityInCart(String id, Integer quantity) {
        UserDetails user = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user1 = findOrThrow(user.getUsername());
        Optional<Cart> cart = cartRepository.findByUser(user1);
        if (cart.isPresent()){for (CartItem item : cart.get().getCartItems()) {
            if (item.getProduct().getId().equals(id)) {
                cart.get().setTotal(cart.get().getTotal()-item.getTotal());
                if (quantity>0){
                    item.setQuantity(quantity);
                }else {
                    throw new RuntimeException("not allowed");
                }
                item.setTotal(item.getProduct().getPrice() * item.getQuantity());
                cart.get().setTotal(cart.get().getTotal()+item.getTotal());
                cartRepository.save(cart.get());

            }
        }

        }
    }
}