package com.example.shoppingplatfrom.cart.repository;

import com.example.shoppingplatfrom.User.model.User;
import com.example.shoppingplatfrom.cart.model.Cart;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface CartRepository extends MongoRepository<Cart,String> {

    Optional<Cart> findById(String s);

    Optional<Cart>  findByUser(User user);

    Cart findByUserEmail(String email);

    //Cart findByCartByUser(User user);

    // Cart findByUser1(User user1);
}
