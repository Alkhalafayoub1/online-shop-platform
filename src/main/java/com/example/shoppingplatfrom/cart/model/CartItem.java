package com.example.shoppingplatfrom.cart.model;

import com.example.shoppingplatfrom.product.model.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.DBRef;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CartItem {

    @DBRef
    private Product product;
    private Integer quantity;
    private Double total;
}
