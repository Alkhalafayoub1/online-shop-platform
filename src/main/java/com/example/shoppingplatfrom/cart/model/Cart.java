package com.example.shoppingplatfrom.cart.model;

import com.example.shoppingplatfrom.User.model.User;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.BindableMongoExpression;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Cart {
    @Id
    @Indexed(unique = true)
    private String id;
    @DBRef
    private User user;

    private Double total;
    List<CartItem>cartItems=new ArrayList<>();
}
