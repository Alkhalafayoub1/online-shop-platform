package com.example.shoppingplatfrom.cart.dto;

import com.example.shoppingplatfrom.cart.model.CartItem;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CartRequest {

    CartItemRequest cartItemRequest;
}
