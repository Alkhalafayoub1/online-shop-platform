package com.example.shoppingplatfrom.cart.controller;

import com.example.shoppingplatfrom.cart.dto.CartRequest;
import com.example.shoppingplatfrom.cart.service.CartService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/cart")
public class CartController {
    private final CartService cartService;
    //UserDetails userDetails = (UserDetails) authentication.getPrincipal()
    @PostMapping("/add-to-cart")

    public ResponseEntity<?> addToCart(@RequestBody CartRequest  cartRequest){
        cartService.addToCart(cartRequest);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
  // @PutMapping("/{id}")
  // public void putCart(
  //         @RequestParam("itemId") String itemId,
  //         @RequestParam("cartId") String  cartId
  // ) {

  //     cartService.updateCart(itemId, cartId);
  // }

    @PutMapping("/delete-item")
    public void deleteItemFromCart(@RequestParam("id") String id){
       cartService.deleteItemFromCart(id);

    }
 @PutMapping("/modify-quantity")
 public void modifyQuantityInCart(
          @RequestParam("id") String id,
          @RequestParam("quantity") Integer quantity){
     cartService.modifyQuantityInCart(id,quantity);
 }

}
