package com.example.shoppingplatfrom.User.service;

import com.example.shoppingplatfrom.User.auth.RegisterRequest;
import com.example.shoppingplatfrom.User.model.User;
import com.example.shoppingplatfrom.User.repository.UserRepository;
import com.example.shoppingplatfrom.domainpermitive.dto.AddressRequest;
import com.example.shoppingplatfrom.domainpermitive.exception.UserAlreadyExistsException;
import com.example.shoppingplatfrom.domainpermitive.exception.UserNotFoundException;
import com.example.shoppingplatfrom.domainpermitive.model.Address;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
   private final ModelMapper mapper;
   private final PasswordEncoder passwordEncoder;

    public void add(RegisterRequest registerRequest) {
        Optional<User> theUser = userRepository.findByEmail(registerRequest.getEmail());

        if (theUser.isPresent()){
            throw new UserAlreadyExistsException("A user with "+registerRequest.getEmail() +" already exists");
        }else {
            Address  address=convertToAddress(registerRequest.getAddressRequest());
            if (registerRequest.getPassword()!=registerRequest.getConfirmPassword()){
                throw new RuntimeException("password ist not matched" );
            } else if (address.getCity().isEmpty()){
                throw new RuntimeException("select a city" );
            } else if (address.getStreet().isEmpty()) {
                throw new RuntimeException("select a street");
            } else if (address.getZipCode().isEmpty()) {
                throw new RuntimeException("select a post code");
            } else if (address.getHouseNr().isEmpty()) {
                throw new RuntimeException("select a House number");
            }
            User user=convertToUser(registerRequest);
            userRepository.save(user);
        }
    }


    public List<User> getAllUsers() {
        return userRepository.findAll()
                .stream()
                .map(user -> mapper.map(user,User.class)).collect(Collectors.toList());
    }

    private User convertToUser(RegisterRequest registerRequest) {
        return User.builder()
                .email(registerRequest.getEmail())
                .firstName(registerRequest.getFirstName())
                .lastName(registerRequest.getLastName())
                .password(passwordEncoder.encode(registerRequest.getPassword()))
                .confirmPassword(passwordEncoder.encode(registerRequest.getConfirmPassword()))
                .role(registerRequest.getRole())
                .address(convertToAddress(registerRequest.getAddressRequest()))
                .userStatus(registerRequest.getUserStatus())
                .build();
    }
    private Address convertToAddress(AddressRequest addressRequest){
        return Address.builder()
                .street(addressRequest.getStreet())
                .city(addressRequest.getCity())
                .Country(addressRequest.getCountry())
                .zipCode(addressRequest.getZipCode())
                .houseNr(addressRequest.getHouseNr())
                .build();

    }
    public void changeAddress(RegisterRequest registerRequest) {
        Optional<User> theUser = userRepository.findByEmail(registerRequest.getEmail());
        if (theUser.isEmpty()){
            throw new UserNotFoundException("A user with " +registerRequest.getEmail() +" is not existed");
        } else {
            Address address = convertToAddress(registerRequest.getAddressRequest());
            if (address.getCity().isEmpty()) {
                throw new RuntimeException("select a city");
            } else if (address.getStreet().isEmpty()) {
                throw new RuntimeException("select a street");
            } else if (address.getZipCode().isEmpty()) {
                throw new RuntimeException("select a post code");
            } else if (address.getHouseNr().isEmpty()) {
                throw new RuntimeException("select a House number");
            }

            theUser.get().setAddress(address);

            userRepository.save(theUser.get());

        }
    }
    public void changePassword(RegisterRequest registerRequest) {}


    @Transactional
    public void delete(String email) {
        userRepository.deleteByEmail(email);
    }

    public User getUser(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new UserNotFoundException("User not found"));
    }


    public User update(User user) {
        user.setRole(user.getRole());
        return userRepository.save(user);
    }

}
