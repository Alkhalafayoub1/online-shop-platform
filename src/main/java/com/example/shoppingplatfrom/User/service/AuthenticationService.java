package com.example.shoppingplatfrom.User.service;

import com.example.shoppingplatfrom.User.auth.AuthenticationRequest;
import com.example.shoppingplatfrom.User.auth.AuthenticationResponse;
import com.example.shoppingplatfrom.User.config.JwtService;
import com.example.shoppingplatfrom.User.model.User;
import com.example.shoppingplatfrom.User.repository.UserRepository;
import com.example.shoppingplatfrom.User.token.Token;
import com.example.shoppingplatfrom.User.token.TokenRepository;
import com.example.shoppingplatfrom.User.token.TokenType;
import com.example.shoppingplatfrom.domainpermitive.dto.AddressRequest;
import com.example.shoppingplatfrom.domainpermitive.exception.UserNotFoundException;
import com.example.shoppingplatfrom.domainpermitive.model.Address;
import lombok.RequiredArgsConstructor;

import org.modelmapper.ModelMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final TokenRepository tokenRepository;
    private final JwtService jwtService ;
    private final AuthenticationManager authenticationManager;
    private final ModelMapper mapper;

  //  public AuthenticationResponse register(RegisterRequest request) {
  //      var user= User.builder()
  //              .email(request.getEmail())
  //              .firstName(request.getFirstName())
  //              .lastName(request.getLastName())
  //              .password(passwordEncoder.encode(request.getPassword()))
  //              .role(request.getRole())
  //              .address(convertToAddress(request.getAddressRequest()))
  //              .userStatus(request.getUserStatus())
  //              .build();
  //      userRepository.save(user);
  //      var jwtToken=jwtService.generateToken(user);
//
  //      return AuthenticationResponse.builder()
  //              .token(jwtToken)
  //              .build();
  //  }
    public Address convertToAddress(AddressRequest addressRequest){
        var address=mapper.map(addressRequest, Address.class);
        return address;
    }

   public AuthenticationResponse authenticate(AuthenticationRequest  authRequest) {
       Authentication authentication = authenticationManager
               .authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUserName(), authRequest.getPassword()));
       if (authentication.isAuthenticated()) {
           var user = userRepository.findByEmail(authRequest.getUserName()).orElseThrow();

           var jwtToken = jwtService.generateToken(user.getEmail());
           revokeAllUserTokens(user);
           saveUserToken(user, jwtToken);
           return AuthenticationResponse.builder()
                   .token(jwtToken)
                   .build();
       } else {
               throw new UserNotFoundException("Invalid user credentials");
           }
   }
   private void saveUserToken(User user, String jwtToken) {
       var token = Token.builder()
               .user(user)
               .token(jwtToken)
               .tokenType(TokenType.BEARER)
               .expired(false)
               .revoked(false)
               .build();
       tokenRepository.save(token);
   }

    private void revokeAllUserTokens(User user) {
        var validUserTokens = tokenRepository.findAllValidTokenByUser(user.getId());
        if (validUserTokens.isEmpty())
            return;
        validUserTokens.forEach(token -> {
            token.setExpired(true);
            token.setRevoked(true);
        });
        tokenRepository.saveAll(validUserTokens);
    }
}
