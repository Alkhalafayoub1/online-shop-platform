package com.example.shoppingplatfrom.User.model;

public enum UserStatus {
    NEW,
    ACTIVE,
    BLOCKED,
    BANNED
}
