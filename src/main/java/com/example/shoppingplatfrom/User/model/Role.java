package com.example.shoppingplatfrom.User.model;

public enum Role {
    USER,
    ADMIN,
    VENDOR
}
