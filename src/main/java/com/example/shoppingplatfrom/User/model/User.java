package com.example.shoppingplatfrom.User.model;

import com.example.shoppingplatfrom.domainpermitive.model.Address;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;


@Document(collection = "user")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class  User{
    @Id
    @Indexed(unique = true)
    private String id;
    @Field
    private String firstName;
    @Field
    private String lastName;

    @NonNull
    private String email;
    @JsonIgnore
    @NonNull
    private String password;
    @NonNull
    private String confirmPassword;
    private Address address;

    private Role role;
    private UserStatus userStatus;

}
