package com.example.shoppingplatfrom.User.auth;

import com.example.shoppingplatfrom.User.model.Role;
import com.example.shoppingplatfrom.User.model.UserStatus;
import com.example.shoppingplatfrom.domainpermitive.dto.AddressRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterRequest {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String confirmPassword;
    private Role role;
    private AddressRequest addressRequest;
    private UserStatus userStatus;
}
