package com.example.shoppingplatfrom.User.repository;

import com.example.shoppingplatfrom.User.model.User;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface UserRepository extends MongoRepository<User,Long> {
    Optional<User> findByEmail(String username);

    void deleteByEmail(String email);
}
