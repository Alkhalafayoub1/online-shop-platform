package com.example.shoppingplatfrom.User.controller;


import com.example.shoppingplatfrom.User.auth.AuthenticationRequest;
import com.example.shoppingplatfrom.User.auth.AuthenticationResponse;

import com.example.shoppingplatfrom.User.config.JwtService;
import com.example.shoppingplatfrom.User.service.AuthenticationService;
import com.example.shoppingplatfrom.domainpermitive.exception.UserNotFoundException;
import lombok.RequiredArgsConstructor;

import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {
    private final AuthenticationManager authenticationManager;
    private final JwtService jwtService;
    private final AuthenticationService authenticationService;
  //  @PostMapping("/register")
  //  public ResponseEntity<AuthenticationResponse> register(@RequestBody RegisterRequest request){
  //      return ResponseEntity.ok(authenticationService.register(request));
//
  //  }


 @PostMapping("/authenticate")
 public ResponseEntity<AuthenticationResponse> authenticate(
         @RequestBody AuthenticationRequest request
 ) {
     return ResponseEntity.ok(authenticationService.authenticate(request));
 }

 /*@PostMapping("/authent")
 public AuthenticationResponse getTokenForAuthenticatedUser(@RequestBody AuthenticationRequest authRequest){
     Authentication authentication = authenticationManager
             .authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUserName(), authRequest.getPassword()));

    // System.out.println("Authenticated by"+authRequest.getUserName()+" and "+authRequest.getPassword());
    // System.out.println("isAuthenticated ?"+authentication.isAuthenticated());
     if (authentication.isAuthenticated()){

         var jwtToken=jwtService.generateToken(authRequest.getUserName());
         return AuthenticationResponse.builder()
                 .token(jwtToken)
                 .build();
     }
     else {
         throw new UserNotFoundException("Invalid user credentials");
     }
 }*/
}
