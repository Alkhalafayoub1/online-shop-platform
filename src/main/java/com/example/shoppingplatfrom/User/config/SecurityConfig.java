package com.example.shoppingplatfrom.User.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@RequiredArgsConstructor
public class SecurityConfig {
    private final JwtAuthenticationFilter jwtauthProvider;

    private final AuthenticationProvider authenticationProvider;
    private static final String[] SECURED_URLs = {"/api/admin/**"};

    private static final String[] UN_SECURED_URLs = {
            "/users/**",
            "/auth/**",
            "/api/vendor/auth/**"
    };


    @Bean
        public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
            return http.csrf().disable()
                    .authorizeHttpRequests()
                    .requestMatchers(UN_SECURED_URLs).permitAll().and()
                    .authorizeHttpRequests().requestMatchers(SECURED_URLs)
                    .hasAuthority("ADMIN").anyRequest().authenticated()
                    .and().sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                    .authenticationProvider(authenticationProvider)
                    .addFilterBefore(jwtauthProvider, UsernamePasswordAuthenticationFilter.class)
                    .build();

     //  http.csrf().disable()
     //          .authorizeHttpRequests()
     //          .requestMatchers("/api/**")
     //          .permitAll()
     //          .anyRequest()
     //          .authenticated()
     //          .and()
     //          .sessionManagement()
     //          .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
     //          .and()
     //          .authenticationProvider(authenticationProvider)
     //          .addFilterBefore(jwtauthProvider, UsernamePasswordAuthenticationFilter.class);

     //  return http.build();
    }

}
