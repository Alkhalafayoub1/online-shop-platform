package com.example.shoppingplatfrom.category.service;

import com.example.shoppingplatfrom.category.dto.CategoryRequest;
import com.example.shoppingplatfrom.category.model.Category;
import com.example.shoppingplatfrom.category.repository.CategoryRepository;
import com.example.shoppingplatfrom.domainpermitive.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CategoryService {
    private final CategoryRepository categoryRepository;
    private  final ModelMapper mapper;
    public void createCategory(CategoryRequest categoryRequest) {
        Category category=mapper.map(categoryRequest,Category.class);
        Optional<Category> category1= categoryRepository.findByName(category.getName());
        if (category1.isPresent()){
            throw new RuntimeException("Category is exist");
        }else {
            categoryRepository.save(category);
        }
    }

}
