package com.example.shoppingplatfrom.category.controller;

import com.example.shoppingplatfrom.category.dto.CategoryRequest;
import com.example.shoppingplatfrom.category.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/category")
@RequiredArgsConstructor
public class CategoryController {
    private final CategoryService categoryService;
     @PostMapping("/addcategory")
    public ResponseEntity<?> addCategory(@RequestBody CategoryRequest categoryRequest){
         categoryService.createCategory(categoryRequest);
         return ResponseEntity.status(HttpStatus.CREATED).build();
     }
}
